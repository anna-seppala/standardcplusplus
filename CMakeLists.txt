cmake_minimum_required(VERSION 2.8)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_C_COMPILER "/usr/bin/avr-gcc")
set(CMAKE_CXX_COMPILER "/usr/bin/avr-g++")
project(stdlib-avr C CXX)
set(CMAKE_BUILD_TYPE "Release")
file (GLOB lib_cpp
  "src/*.cpp"
  )
add_library(${PROJECT_NAME} STATIC ${lib_cpp})
set_target_properties(
  ${PROJECT_NAME}
  PROPERTIES
  COMPILE_FLAGS "-mmcu=atmega328p"
  OUTPUT_NAME ${PROJECT_NAME}
  )

target_include_directories(${PROJECT_NAME} PUBLIC include)

install(TARGETS ${PROJECT_NAME} DESTINATION $ENV{OWN_LIB_DIR}/${PROJECT_NAME}/)
file(GLOB lib_hdrs
  "include/*"
  )
install(FILES ${lib_hdrs} DESTINATION $ENV{OWN_INCLUDE_DIR}/${PROJECT_NAME}/)

